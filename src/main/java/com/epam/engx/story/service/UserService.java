package com.epam.engx.story.service;

import com.epam.engx.story.domain.User;

public interface UserService {
    User saveUser(User user);

    User getUser(int id);

    // Additional CRUD operations
}
