package com.epam.engx.story.repository;

import java.util.Map;
import java.util.Optional;

import com.epam.engx.story.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "storage", name = "external", havingValue = "false")
public class InMemoryUserRepository implements UserRepository {
    private final Map<Integer, User> users;

    @Override
    public User save(User entity) {
        return users.put(entity.getId(), entity);
    }

    @Override
    public Optional<User> findById(int id) {
        return Optional.ofNullable(users.get(id));
    }

}
