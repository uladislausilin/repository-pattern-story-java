package com.epam.engx.story.repository;

import java.sql.Statement;
import java.util.Optional;

import com.epam.engx.story.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "storage", name = "external", havingValue = "true")
public class DbUserRepository implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    @Override
    public User save(User user) {
        var query = "INSERT INTO users_db.users (name, email) VALUES (?, ?)";
        var keyHolder = new GeneratedKeyHolder();
        var id = jdbcTemplate.update(connection -> {
            var ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getName());
            ps.setString(2, user.getEmail());
            return ps;
        }, keyHolder);
        return findById(id).orElseThrow(() -> new RuntimeException("Could not find"));
    }

    @Override
    public Optional<User> findById(int id) {
        var query = "SELECT * FROM users_db.users WHERE id = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(
                query,
                (rs, rowNum) -> User.builder()
                        .id(rs.getInt("id"))
                        .name(rs.getString("name"))
                        .email(rs.getString("email"))
                        .build(),
                id
        ));
    }
}
