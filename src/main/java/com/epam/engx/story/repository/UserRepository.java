package com.epam.engx.story.repository;

import java.util.Optional;

import com.epam.engx.story.domain.User;

public interface UserRepository {

    User save(User user);

    Optional<User> findById(int id);

    // Additional CRUD operations

}
