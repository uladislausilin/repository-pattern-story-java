package com.epam.engx.story;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.epam.engx.story.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@SpringBootApplication
public class RepositoryPatternStoryJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepositoryPatternStoryJavaApplication.class, args);
    }

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:jdbc/schema.sql").build();
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public Map<Integer, User> users() {
        return new HashMap<>(Map.of(1, User.builder().id(1).name("Test1").email("test1@example.com").build()));
    }
}
