CREATE SCHEMA IF NOT EXISTS users_db AUTHORIZATION sa;

CREATE TABLE IF NOT EXISTS users_db.users (
    id int auto_increment primary key,
    name varchar(50),
    email varchar(50)
);