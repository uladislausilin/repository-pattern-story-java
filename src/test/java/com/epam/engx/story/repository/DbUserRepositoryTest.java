package com.epam.engx.story.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.epam.engx.story.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = "storage.external=true")
class DbUserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User expectedUser;


    private static User buildTestUser() {
        return User.builder().name("Test1").email("test1@example.com").build();
    }

    @BeforeEach
    void setUp() {
        expectedUser = userRepository.save(buildTestUser());
    }

    @Test
    void findUserTest() {
        var user = userRepository.findById(expectedUser.getId()).orElse(User.builder().build());

        assertInstanceOf(DbUserRepository.class, userRepository);

        assertNotNull(user);
        assertEquals(expectedUser.getId(), user.getId());
        assertEquals(expectedUser.getName(), user.getName());
        assertEquals(expectedUser.getEmail(), user.getEmail());
    }

}