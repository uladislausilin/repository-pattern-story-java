package com.epam.engx.story.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.epam.engx.story.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = "storage.external=false")
class InMemoryUserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    private static User buildTestUser() {
        return User.builder().id(1).name("Test1").email("test1@example.com").build();
    }

    @Test
    void findUserTest() {
        var expectedUser = buildTestUser();
        var user = userRepository.findById(expectedUser.getId()).orElse(User.builder().build());

        assertInstanceOf(InMemoryUserRepository.class, userRepository);

        assertNotNull(user);
        assertEquals(expectedUser.getId(), user.getId());
        assertEquals(expectedUser.getName(), user.getName());
        assertEquals(expectedUser.getEmail(), user.getEmail());
    }

}