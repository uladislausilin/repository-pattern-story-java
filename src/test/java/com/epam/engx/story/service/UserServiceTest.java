package com.epam.engx.story.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import com.epam.engx.story.repository.UserRepository;
import com.epam.engx.story.domain.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;

    private static User buildTestUser() {
        return User.builder().id(1).name("Test1").email("test1@example.com").build();
    }

    @Test
    @DisplayName("Find User")
    void getUser() {
        var expectedUser = buildTestUser();
        doReturn(Optional.of(expectedUser)).when(userRepository).findById(anyInt());

        var user = userService.getUser(1);

        assertNotNull(user);
        assertEquals(expectedUser.getId(), user.getId());
        assertEquals(expectedUser.getName(), user.getName());
        assertEquals(expectedUser.getEmail(), user.getEmail());
    }

    @Test
    @DisplayName("Save User")
    void saveUser() {
        var expectedUser = buildTestUser();
        doReturn(expectedUser).when(userRepository).save(any());

        var user = userService.saveUser(expectedUser);

        assertNotNull(user);
        assertEquals(expectedUser.getId(), user.getId());
        assertEquals(expectedUser.getName(), user.getName());
        assertEquals(expectedUser.getEmail(), user.getEmail());
    }
}